/**
 * 我的地盘
 *
 * @export
 * @interface IbzMyTerritory
 */
export interface IbzMyTerritory {

    /**
     * fails
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    fails?: any;

    /**
     * 通讯地址
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    address?: any;

    /**
     * 密码
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    password?: any;

    /**
     * 微信
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    weixin?: any;

    /**
     * 钉钉
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    dingding?: any;

    /**
     * 账户
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    account?: any;

    /**
     * ranzhi
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    ranzhi?: any;

    /**
     * slack
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    slack?: any;

    /**
     * 真实姓名
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    realname?: any;

    /**
     * locked
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    locked?: any;

    /**
     * scoreLevel
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    scorelevel?: any;

    /**
     * avatar
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    avatar?: any;

    /**
     * zipcode
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    zipcode?: any;

    /**
     * 所属部门
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    dept?: any;

    /**
     * 源代码账户
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    commiter?: any;

    /**
     * 逻辑删除标志
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    deleted?: any;

    /**
     * 最后登录
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    last?: any;

    /**
     * skype
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    skype?: any;

    /**
     * score
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    score?: any;

    /**
     * whatsapp
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    whatsapp?: any;

    /**
     * 访问次数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    visits?: any;

    /**
     * 手机
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    mobile?: any;

    /**
     * clientLang
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    clientlang?: any;

    /**
     * 入职日期
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    join?: any;

    /**
     * ip
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    ip?: any;

    /**
     * 邮箱
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    email?: any;

    /**
     * nickname
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    nickname?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    phone?: any;

    /**
     * birthday
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    birthday?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    id?: any;

    /**
     * QQ
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    qq?: any;

    /**
     * 男女
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    gender?: any;

    /**
     * 职位
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    role?: any;

    /**
     * clientStatus
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    clientstatus?: any;

    /**
     * 我的任务
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    mytasks?: any;

    /**
     * 我的bugs
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    mybugs?: any;

    /**
     * 我的过期bug数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    myebugs?: any;

    /**
     * 我的需求数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    mystorys?: any;

    /**
     * 未关闭产品数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    products?: any;

    /**
     * 过期项目数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    eprojects?: any;

    /**
     * 未关闭项目数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    projects?: any;

    /**
     * 我的过期任务数
     *
     * @returns {*}
     * @memberof IbzMyTerritory
     */
    myetasks?: any;
}