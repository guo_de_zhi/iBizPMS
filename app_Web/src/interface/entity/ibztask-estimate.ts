/**
 * 任务预计
 *
 * @export
 * @interface IBZTaskEstimate
 */
export interface IBZTaskEstimate {

    /**
     * 用户
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    account?: any;

    /**
     * 预计剩余
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    left?: any;

    /**
     * 总计消耗
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    consumed?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    id?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    date?: any;

    /**
     * work
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    work?: any;

    /**
     * 任务
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    task?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof IBZTaskEstimate
     */
    dates?: any;
}