/**
 * 任务预计
 *
 * @export
 * @interface TaskEstimate
 */
export interface TaskEstimate {

    /**
     * 用户
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    account?: any;

    /**
     * 预计剩余
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    left?: any;

    /**
     * 总计消耗
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    consumed?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    id?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    date?: any;

    /**
     * work
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    work?: any;

    /**
     * 任务
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    task?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof TaskEstimate
     */
    dates?: any;
}