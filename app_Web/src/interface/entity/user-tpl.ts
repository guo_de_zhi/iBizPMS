/**
 * 用户模板
 *
 * @export
 * @interface UserTpl
 */
export interface UserTpl {

    /**
     * title
     *
     * @returns {*}
     * @memberof UserTpl
     */
    title?: any;

    /**
     * id
     *
     * @returns {*}
     * @memberof UserTpl
     */
    id?: any;

    /**
     * content
     *
     * @returns {*}
     * @memberof UserTpl
     */
    content?: any;

    /**
     * type
     *
     * @returns {*}
     * @memberof UserTpl
     */
    type?: any;

    /**
     * account
     *
     * @returns {*}
     * @memberof UserTpl
     */
    account?: any;

    /**
     * public
     *
     * @returns {*}
     * @memberof UserTpl
     */
    ibizpublic?: any;
}