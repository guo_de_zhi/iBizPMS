export default {
  fields: {
    pathname: "路径",
    objectid: "对象ID",
    deleted: "已删除",
    extension: "文件类型",
    objecttype: "对象类型",
    addedby: "由谁添加",
    title: "标题",
    addeddate: "添加时间",
    downloads: "下载次数",
    size: "大小",
    id: "id",
    extra: "备注",
  },
	views: {
		listview9: {
			caption: "附件",
      		title: "附件",
		},
	},
};