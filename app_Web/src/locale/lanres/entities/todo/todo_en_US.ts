
export default {
  fields: {
    id: "编号",
    account: "所有者",
    closeddate: "关闭时间",
    closedby: "由谁关闭",
    type: "类型",
    end: "结束",
    desc: "描述",
    finishedby: "由谁完成",
    begin: "开始",
    idvalue: "关联编号",
    assignedby: "由谁指派",
    finisheddate: "完成时间",
    cycle: "周期",
    assignedto: "指派给",
    status: "状态",
    name: "待办名称",
    assigneddate: "指派日期",
    pri: "优先级",
    date: "日期",
    ibizprivate: "私人事务",
    config: "config",
    config_day: "间隔天数",
    config_beforedays: "提前",
    config_week: "周期设置周几",
    config_month: "周期设置月",
    config_type: "周期类型",
    config_end: "过期时间",
    bug: "待办名称",
    task: "待办名称",
    story: "待办名称",
    date1: "日期",
    date_disable: "待定",
  },
	views: {
		baseeditview9: {
			caption: "基本信息",
      		title: "待办事宜表编辑视图",
		},
		assigntoview: {
			caption: "指派给",
      		title: "指派表单视图",
		},
		desceditview9: {
			caption: "基本信息",
      		title: "待办事宜表编辑视图",
		},
		todocreateview: {
			caption: "待办事宜表",
      		title: "添加待办",
		},
		gridview: {
			caption: "待办事宜表",
      		title: "待办事宜表格视图",
		},
		gridview9: {
			caption: "我的待办",
      		title: "我的待办",
		},
		editview: {
			caption: "待办事宜表",
      		title: "编辑待办",
		},
		dashboardview: {
			caption: "待办事宜表",
      		title: "待办事宜表数据看板视图",
		},
	},
	dashboardbasic_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "周期", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "待办名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			pri: "优先级", 
			status: "状态", 
			type: "类型", 
			account: "所有者", 
			date: "日期", 
			begin: "起止时间", 
			end: "~", 
			assignedto: "指派给", 
			assigneddate: "指派日期", 
			config_type: "周期类型", 
			date1: "起止时间", 
			config_end: "~", 
			config_day: "周期设置", 
			config_week: "周期设置", 
			config_month: "周期设置", 
			config_beforedays: "提前", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	dashboardmain_form: {
		details: {
			grouppanel1: "分组面板", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "待办名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "描述", 
			desc: "", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	editform_form: {
		details: {
			grouppanel3: "分组面板", 
			grouppanel2: "周期设置", 
			grouppanel1: "分组面板", 
			group1: "编辑待办", 
			formpage1: "添加待办", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "待办名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			date: "日期", 
			cycle: "周期", 
			config_type: "周期类型", 
			config_day: "", 
			config_week: "", 
			config_month: "", 
			config_beforedays: "提前", 
			formitem: "天生成待办", 
			config_end: "过期时间", 
			type: "类型", 
			pri: "优先级", 
			name: "待办名称", 
			desc: "描述", 
			status: "状态", 
			begin: "起止时间", 
			end: "~", 
			formitem10: "", 
			private: "私人事务", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	assigntoform_form: {
		details: {
			group1: "指派给", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "待办名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			assignto: "指派给", 
			date: "日期", 
			future: "", 
			begin: "起止时间", 
			end: "~", 
			lbldisabledate: "", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			grouppanel3: "分组面板", 
			grouppanel1: "周期设置", 
			grouppanel2: "分组面板", 
			group1: "", 
			formpage1: "添加待办", 
			srforikey: "", 
			srfkey: "编号", 
			srfmajortext: "待办名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			date: "日期", 
			date_disable: "", 
			cycle_enable: "", 
			config_type: "周期类型", 
			idvalue: "关联编号", 
			formitem3: "", 
			formitem4: "", 
			formitem5: "", 
			formitem2: "提前", 
			formitem6: "天生成待办", 
			formitem: "过期时间", 
			type: "类型", 
			pri: "优先级", 
			task: "待办名称", 
			story: "待办名称", 
			bug: "待办名称", 
			name: "待办名称", 
			formitem1: "描述", 
			status: "状态", 
			begin: "起止时间", 
			end: "~", 
			formitem10: "", 
			private: "私人事务", 
			id: "编号", 
		},
		uiactions: {
		},
	},
	myupcoming_grid: {
		columns: {
			date1: "日期",
			begin: "开始",
			pri: "优先级",
			name: "待办名称",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			id: "编号",
			date1: "日期",
			type: "类型",
			pri: "优先级",
			name: "待办名称",
			begin: "开始",
			end: "结束",
			status: "状态",
			uagridcolumn1: "操作",
		},
		uiactions: {
        todo_assignto: "指派",
        todo_finish: "完成",
        todo_activate: "激活",
        todo_close: "关闭",
        todo_edit1: "编辑",
        todo_delete: "删除",
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	todocreateviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
};